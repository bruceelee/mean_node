#APLICACION WEB PARA SISTEMA DE PRODUCCION DE RANDOM ERP

## Instalación
1. Descargar el repositorio
2. Instalar modulos npm: 'npm install'
3. Instalar dependencias bower: 'bower install'
4. Comenzar el servidor: 'node server.js'
5. Acceder desde un navegador a http://localhost:8080

##Qué se lleva
1. Se logro una arquitectura MEAN
2. Se logro una estructura como la existente en Laravel Framework
3. Estructura basica de modelo y controladores de User y routes

###Qué falta
1. Login, datos de User
2. Manejo de sesiones
3. Modelo, controlador, vista de Tareas
4. Conexion con ODCB


Agradecimientos a [chris@scotch.io](mailto:chris@scotch.io) por su proyecto MEAN Stack Single Page Application Starter: 

https://scotch.io/tutorials/setting-up-a-mean-stack-single-page-application

https://github.com/scotch-io/starter-node-angular

