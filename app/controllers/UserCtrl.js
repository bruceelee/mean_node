var mongoose = require('mongoose');
var User  = require('../models/User');


		// create a user (accessed at POST http://localhost:8080/users)
exports.post =function(req, res) {
			
			var user = new User();		// create a new instance of the User model
			user.name = req.query.name;  // set the users name (comes from the request)
			
			user.save(function(err) {
				if (err)
					res.send(err);

				res.json({ message: 'User created!'});
			});

			
		};

		// get all the users (accessed at GET http://localhost:8080/api/users)
exports.get= function(req, res) {

			User.find(function(err, users) {
				if (err)
					res.send(err);

				res.json(users);
			});
		};

// get the user with that id
exports.getid= function(req, res) {
			User.findById(req.params.user_id, function(err, user) {
				if (err)
					res.send(err);
				res.json(user);
			});
		};

		// update the user with this id
exports.putid= function(req, res) {
			User.findById(req.params.user_id, function(err, user) {

				if (err)
					res.send(err);

				user.name = req.body.name;
				user.save(function(err) {
					if (err)
						res.send(err);

					res.json({ message: 'User updated!' });
				});

			});
		};

		// delete the user with this id
exports.deleteid= function(req, res) {
			User.remove({
				_id: req.params.user_id
			}, function(err, user) {
				if (err)
					res.send(err);

				res.json({ message: 'Successfully deleted' });
			});
		};