var express    = require('express');

module.exports = function(app) {

	// Creo enrutador
	var router = express.Router();

	// middleware to use for all requests
	router.use(function(req, res, next) {
		// do logging
		console.log('Something is happening.');
		next();
	});

	
	// ----------------------------------------------------
	//Asocio las rutas a los controladores
	var users= require('../app/controllers/UserCtrl');

	router.route('/users').get(users.get);
	router.route('/users').post(users.post);
	router.route('/users/:user_id').get(users.getid);
	router.route('/users/:user_id').put(users.putid);
	router.route('/users/:user_id').delete(users.deleteid);

	// REGISTRO EL ROUTER A LA APP -------------------------------
	app.use('/api', router);
 	
};