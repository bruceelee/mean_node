angular.module('UserService', [])


.factory('User', ['$http', function($http) {

	  return {
        // call to get all nerds
        get : function() {
            return $http.get('/api/users');
        },
                // these will work when more API routes are defined on the Node side of things
        // call to POST and create a new nerd
        create : function(userData) {
            return $http.post('/api/users', userData);
        },

        // call to DELETE a nerd
        delete : function(id) {
            return $http.delete('/api/users/' + id);
        }
	}
}])


//Creo filtro para ng-repeat, startFrom permite inicializar pagination
.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
})

//Traduzco botones de accion del pagination
.run(function(paginationConfig){
   paginationConfig.firstText='Primero';
   paginationConfig.previousText='Anterior';
   paginationConfig.lastText='Último';
   paginationConfig.nextText='Siguiente';

});