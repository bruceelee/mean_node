angular.module('UserCtrl', ['ui.bootstrap'])

.controller('UserController',  function($scope, User, $location, $log, $modal) {

  /***********Inicio variables *************************************/
  
  $scope.tagline = 'Nothing beats a pocket protector!';

  //Para hacer un listado de las alertas
  $scope.alerts = [];   //Las alertas son objetos {type:'', msg: ''}
  
  //Variables para manejar el listado de users
  $scope.users=[];   
  $scope.newUser={username:"", email:"", direccion:"",password:""};

  //Obtengo los usuarios
  User.get(function(data){    //Uso comando GET, gracias a app/services/services por Angular, y REST por laravel
    $scope.users= data.users;
  });

  //Variables para paginado
   $scope.entryLimit= 10;
   $scope.currentPage=1;
   $scope.maxSize=10;
 


  /********************Funciones para manejar el Paginado****************************/
  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };

  $scope.pageChanged = function() {
    $log.log('Page changed to: ' + $scope.currentPage);
  };
	
  
  /*******************Funciones para manejar alertas*****************************/
	$scope.addAlert = function(alert) {
    $scope.alerts.push({ type: alert.type, msg: alert.msg});
  };

  $scope.closeAlert = function(index) {
    	$scope.alerts.splice(index, 1);
  	};


  /******************Funciones para CRUD de usuarios******************************/
  	$scope.agregarUsuario= function(){

      if ($scope.userForm.$valid) {
    		console.log($scope.newUser.username);

    		var usuario= new User;
    		usuario.username= $scope.newUser.username;
        usuario.email= $scope.newUser.email;
        usuario.direccion= $scope.newUser.direccion;
        usuario.password= $scope.newUser.password;

        //Invoco un comando POST a /user, que invoca la función @store. Esto es gracias a REST de Laravel
        usuario.$save(function(data){
          $scope.alerts.push({type:'success', msg: "Usuario guardado con exito"});

          //Hay dos opciones: 1.- Recargar la lista de usuarios.
          //          2.- Buscar usuario por username, y agregarlo a la lista (crear nuevo metodo o username fijarlo como llave).
         
          User.get(function(data){    
            $scope.users= data.users;
          });

          $scope.reset();
        },
        function(data){
          $scope.alerts.push({type:'danger', msg:data.data.message});
        });

        }
    
    }

    $scope.visualizarUsuario=function(userid){

           $location.path('/perfil').search('user', userid);
    }

    $scope.eliminarUsuario= function(userid,index){

      //Uso comando DELETE, gracias a app/services/services por Angular, y REST por laravel
      User.delete({id: userid}, function(data){  
      
        if(data.error==false){

          for(var i=0, find=false; i<$scope.users.length && find==false; i++){
              if ($scope.users[i].id== userid){
                  $scope.users.splice(i,1);
                  find=true;
              }
          }
        }
         console.log(data.message);
         
        });
    }

    $scope.reset = function() {
    
      $scope.newUser = {username:"", email:"", direccion:"",password:""};

    }


    $scope.openModal_editarUsuario= function (size, userid) {

      var modalInstance = $modal.open({
        templateUrl: 'myModalContent.html',
        controller: 'ModalInstanceCtrl',
        size: size,
        resolve: {
          id: function () {
            return userid;
          }
        }
      });

      modalInstance.result.then(function (resultado) {  //Cuando una ventana Modal se cierra, envia un resultado que se recibe aca
          
        console.log(resultado);
        
        if(resultado.error==false){
          //Hago un refresh del user editado en users
          for(var i=0, find=false; i<$scope.users.length && find==false; i++){
              if ($scope.users[i].id== resultado.user.id){
                  $scope.users[i]= resultado.user;
                  find=true;
              }
          }
        }
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
  };   //fin $scope.editarUsuario
  	
	
})

.controller('ModalInstanceCtrl', function ($scope, $resource, $modalInstance, id, User) {

    $scope.checked=true;
    User.get({id: id}, function(data){      //Invoco a show de REST de Laravel
      
        if(data.error==false){
          $scope.edit_user=data.user;
        }
         console.log(data.message);
         
   });  //Fin de User.get({id: id}, function(data){

   $scope.editarUsuario = function () {
      console.log($scope.edit_user);
      var User_edit = $resource('/user/:id/edit?direccion='+$scope.edit_user.direccion+
                                                        '&email='+$scope.edit_user.email+
                                                        '&username='+$scope.edit_user.username
                                                        ,{id:'@id'});
      User_edit.get({id:id}, function(data){
      

      console.log(data.message);
      if(data.error!=true)
        $modalInstance.close({error:data.error, user:$scope.edit_user});   //close(result), en el cierre envio una respuesta al padre
      });
    
   };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };


})
